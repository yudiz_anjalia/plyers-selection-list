import data from '../data/players.json'
// import roles from '../data/player_roles.json';

function randomDataAsync(data)
{
    for (var a = 0; a < data.length; a++) {
        var x = data[a];
        var y = Math.floor(Math.random() * (a + 1));
        data[a] = data[y];
        data[y] = x;
    }
    return data;
}


export async function getPlayerRoles()
{
    const wicket_keeper = 2;
    const all_rounder = 2;
    const bats_man = 4;
    const for_bowling = 3;
    const getplayers = [];

    let WK = 0, ALLR = 0, BATS = 0, BWL = 0;
    const player = randomDataAsync(data);

    player.map(players => {
        if(players.player_role === 'WK' && WK < wicket_keeper)
        {
            getplayers.push(players);
            WK += 1;
        }

        if (players.player_role === 'ALLR' && ALLR < all_rounder)
        {
            getplayers.push(players);
            ALLR += 1;
        }

        if (players.player_role === 'BATS' && BATS < bats_man)
        {
            getplayers.push(players);
            BATS += 1;
        }
        
        if (players.player_role === 'BWL' && BWL < for_bowling)
        {
            getplayers.push(players);
            BWL += 1;
        }
    });
    return getplayers;
}