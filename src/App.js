import './App.css';
import React from "react";
import PlayerSelection from './components/index';

function App() {
  return (

    <div className="App">
      <PlayerSelection />
    </div>
    
  );
}

export default App;
