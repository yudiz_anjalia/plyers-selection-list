import React, { useState, useEffect } from "react";
import { getPlayerRoles } from "./MainPage";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Helmet from 'react-helmet';

function PlayerSelection() {
  const [ players, setPlayers ] = useState([]);

  useEffect(()=>{
    getPlayerRoles().then(players=>{
      setPlayers(players);
    })
  },[]);

  return (
    <>
    {/* HELMET */}
    <Helmet>
        <title>Sports Selection List</title>
        <meta name='description' content='Sports players selection list' />
    </Helmet>


    <div>
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">

        <TableHead>
          <TableRow>
            <TableCell><b>Player ID</b></TableCell>
            <TableCell><b>Player Name</b></TableCell>
            <TableCell><b>Player Role</b></TableCell>
            <TableCell><b>Team ID</b></TableCell>
            <TableCell><b>Player Credits</b></TableCell>
          </TableRow>
        </TableHead>

        <TableBody>
          {players.map((row) => (
            <TableRow key={row.name} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
              <TableCell component="th" scope="row">
                {row.player_id}
              </TableCell>
              <TableCell>{row.player_name}</TableCell>
              <TableCell>{row.player_role}</TableCell>
              <TableCell>{row.team_id}</TableCell>
              <TableCell>{row.player_credits}</TableCell>
            </TableRow>
          ))}
        </TableBody>

      </Table>
    </TableContainer>
    </div>
    </>
  );
}

export default PlayerSelection;